:: Start MongoDB
start cmd.exe /k "mongod"

:: cd front end directory, and start it
cd frontend
start cmd.exe /k "npm start"

:: cd to api server, and start that
cd ../backend
start cmd.exe /k "sbt run"