/**
 * Created by LeutrimNeziri on 30/03/2019.
 */
module.exports = {
  API_URL: 'http://localhost:9000/api',
  PAGES: {
    HOME: 'home',
    LECTURE_1: {
      ID: 'lecture1',
      GETTING_STARTED: 'gettingStarted',
      PROJECT_SETTUP: 'projectSetup',
      AGILE_METHODOLOGY: 'agile',
      WAY_OF_WORKING: 'wayofWorking',
      WORKING_WITH_GIT: 'git'
    },
    LECTURE_2: {
      ID: 'lecture2',
      JAVA_INTRODUCTION: 'JavaIntroduction',
      OOP: 'OOP Principles',
      OOP_MISC: 'OOP Principles Misc',
      JAVA_RELEASES: 'Java New Features',
      ASSIGNMENTS: 'assignments_two'
    },
    LECTURE_3: {
      ID: 'lecture3',
      PLAY_INTRODUCTION: 'PlayIntroduction',
      REST_API: 'Rest API'
    },
    LECTURE_4: {
      ID: 'lecture4',
      PLAY_ROUTING: 'Routing',
      MANIPULATING_RESPONSES: 'Manipulating Responses',
      BODY_PARSERS: 'Body Parsers',
      HANDLING_SERVING_JSON: 'JSON Handling',
      ASSIGNMENTS: 'assignments_four'
    },
    LECTURE_5: {
      ID: 'lecture5',
      PLAY_ASYNC: 'Handling asynchronous results',
      ACTION_COMPOSITION: 'Action Composition',
      FILE_UPLOADS: 'Handling file uploads',
    },
    LECTURE_6: {
      ID: 'lecture6',
      VALIDATION: 'Data Validation',
      DEPENDENCY_INJECTION: 'Dependency Injection',
      REDIS_CACHING: 'Redis Caching',
      ASSIGNMENTS: 'assignments_six',
    },
    LECTURE_7: {
      ID: 'lecture7',
      INTRODUCTION: 'Akka Introduction',
      AKKA_PLAY: 'Akka and Play'
    },
    PLAYGROUND: 'playground',
    SUPPORT: {
      ID: 'support',
      GLOSSARY: 'glossary',
      RESOURCES: 'resources',
      PLAN_PROGRAM: 'program',
      TIPS_AND_TRICKS: 'tips_and_tricks'
    }
  },
}


