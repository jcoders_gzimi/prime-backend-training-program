/**
 * Created by LeutrimNeziri on 09/04/2019.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import SimpleLink from "presentations/rows/SimpleLink";
import { Bold, Highlighted } from "presentations/Label";
import RouteErrorsPreview from 'assets/images/lecture4/route-errors-preview.png'
import Code from "../../presentations/Code";

const styles = ({ typography }) => ({
  root: {},
})

const inferredContentTypeExample = `Result textResult = ok("Hello World!");`

const toJsonExample = `JsonNode json = Json.toJson(object);
Result jsonResult = ok(json);`

const asKeyword = `Result htmlResult = ok("<h1>Hello World!</h1>").as("text/html");`

const mimeTypes = `Result htmlResult = ok("<h1>Hello World!</h1>").as(MimeTypes.HTML);`

const httpHeaders = `public Result index() {
  return ok("<h1>Hello World!</h1>")
      .as(MimeTypes.HTML)
      .withHeader(CACHE_CONTROL, "max-age=3600")
      .withHeader(ETAG, "some-etag-calculated-value");
}`

const coockiesExample = `public Result index() {
  return ok("<h1>Hello World!</h1>")
      .as(MimeTypes.HTML)
      .withCookies(Cookie.builder("theme", "blue").build());
}`

const detailedExample = `public Result index() {
  return ok("<h1>Hello World!</h1>")
      .as(MimeTypes.HTML)
      .withCookies(
          Cookie.builder("theme", "blue")
              .withMaxAge(Duration.ofSeconds(3600))
              .withPath("/some/path")
              .withDomain(".example.com")
              .withSecure(false)
              .withHttpOnly(true)
              .withSameSite(Cookie.SameSite.STRICT)
              .build());
}`

const discardCoockies = `public Result index() {
  return ok("<h1>Hello World!</h1>").as(MimeTypes.HTML).discardingCookie("theme");
}`

const charsetManipulation = `public Result index() {
  return ok("<h1>Hello World!</h1>", "iso-8859-1")
      .as("text/html; charset=iso-8859-1");
}`

class ManipulatingResponses extends React.Component {
  render() {
    const { classes, section } = this.props
    const defaultContentType = section.children[0]
    const headers = section.children[1]
    const cookies = section.children[2]
    const charsets = section.children[3]
    return (
      <Fragment>
        <Typography variant={'heading'}>
          Section 4: {section.display}
          <Typography>
            Resources: <SimpleLink href="https://www.playframework.com/documentation/2.8.x/JavaResponse">https://www.playframework.com/documentation/2.8.x/JavaResponse</SimpleLink>
          </Typography>
          <Divider />
        </Typography>
        <Typography id={defaultContentType.id} variant={'title'}>
          {defaultContentType.display}
        </Typography>
        <Typography>
          The result content type is automatically inferred from the Java value you specify as response body.
        </Typography>
        <Typography>
          For example:
          <Code>
            {inferredContentTypeExample}
          </Code>
          Will automatically set the <Highlighted>Content-Type</Highlighted> header to <Highlighted>text/plain</Highlighted>, while:
          <Code>
            {toJsonExample}
          </Code>
          will set the <Highlighted>Content-Type</Highlighted> header to <Highlighted>application/json</Highlighted>.
        </Typography>
        <Typography>
          This is pretty useful, but sometimes you want to change it. Just use the <Highlighted>as(newContentType)</Highlighted> method on a result to create a new similar result with a different <Highlighted>Content-Type</Highlighted> header:
          <Code>
            {asKeyword}
          </Code>
          or even better, using:
          <Code>
            {mimeTypes}
          </Code>
        </Typography>
        <Typography id={headers.id} variant={'title'}>
          {headers.display}
        </Typography>
        <Typography>
          You can also add (or update) any HTTP header to the result:
          <Code>
            {httpHeaders}
          </Code>
          Note that setting an HTTP header will automatically discard the previous value if it was existing in the original result.
        </Typography>
        <Typography id={cookies.id} variant={'title'}>
          {cookies.display}
        </Typography>
        <Typography>
          Cookies are just a special form of HTTP headers, but Play provides a set of helpers to make it easier.
        </Typography>
        <Typography>
          You can easily add a Cookie to the HTTP response using:
          <Code>
            {coockiesExample}
          </Code>
          If you need to set more details, including the path, domain, expiry, whether it’s secure, and whether the HTTP only flag should be set, you can do this with the overloaded methods:
          <Code>
            {detailedExample}
          </Code>
          To discard a Cookie previously stored on the web browser:
          <Code>
            {discardCoockies}
          </Code>
          If you set a path or domain when setting the cookie, make sure that you set the same path or domain when discarding the cookie, as the browser will only discard it if the name, path and domain match.
        </Typography>
        <Typography id={charsets.id} variant={'title'}>
          {charsets.display}
        </Typography>
        <Typography>
          For a text based HTTP response it is very important to handle the charset correctly. Play handles that for you and uses <Highlighted>utf-8</Highlighted> by default (see why to use <Highlighted>utf-8</Highlighted>).
        </Typography>
        <Typography>
          The charset is used to both convert the text response to the corresponding bytes to send over the network socket, and to update the <Highlighted>Content-Type</Highlighted> header with the proper <Highlighted>;charset=xxx</Highlighted> extension.
        </Typography>
        <Typography>
          The encoding can be specified when you are generating the Result value:
          <Code>
            {charsetManipulation}
          </Code>
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(ManipulatingResponses)
