/**
 * Created by LeutrimNeziri on 09/04/2019.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import BinarySearchTreeImage from "assets/images/lecture4/binary_search_tree.png";
import SimpleLink from "presentations/rows/SimpleLink";
import faker from 'faker'
import { useDispatch } from 'react-redux'
import { CALL_API } from 'middleware/Api'

import Chart from "presentations/Chart";
import EditIcon from '@material-ui/icons/Edit'
import RemoveIcon from '@material-ui/icons/Clear'
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControlLabel,
  IconButton,
  ListItem,
  Radio,
  RadioGroup,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TextField
} from "@material-ui/core";
import _ from "underscore/modules/index-default";
import withPostRequestTests from "middleware/withPostRequestTests";
import LoadingIndicator from "presentations/LoadingIndicator";
import ErrorBox from "presentations/ErrorBox";
import { Normal } from "presentations/Label";
import Code from "presentations/Code";

const routes = `GET            /api/assignments/lecture3/users                                    @io.training.api.controllers.AssignmentLectureThreeController.all(request: Request)
POST           /api/assignments/lecture3/users                                    @io.training.api.controllers.AssignmentLectureThreeController.save(request: Request)
PUT            /api/assignments/lecture3/users/:id                                @io.training.api.controllers.AssignmentLectureThreeController.update(request: Request, id: String)
DELETE         /api/assignments/lecture3/users/:id                                @io.training.api.controllers.AssignmentLectureThreeController.delete(request: Request, id: String)
GET            /api/assignments/lecture3/users/averages                           @io.training.api.controllers.AssignmentLectureThreeController.averages(request: Request)
GET            /api/assignments/lecture3/users/types                              @io.training.api.controllers.AssignmentLectureThreeController.types(request: Request)
POST           /api/assignments/lecture3/binaryTree                               @io.training.api.controllers.AssignmentLectureThreeController.binaryTree(request: Request)`

const styles = ({ typography, size }) => ({
  graphs: {
    display: 'flex',
    flexFlow: 'row wrap',
    alignItems: 'flex-start',
    alignContent: 'flex-start',
    width: '100%'
  },
  card: {
    position: 'relative',
    backgroundColor: 'white',
    width: `calc(50% - ${size.spacing * 2}px)`,
    margin: size.spacing,
    height: 420,
    padding: 8,
    display: 'flex',
    flexFlow: 'column wrap',
    alignItems: 'flex-start'
  },
  avatar: {
    width: 60,
  },
  graph: {
    display: 'flex',
    flex: 1,
    width: '100%',
    height: 'auto'
  }
})

const model = `{
  id: 'my id', 
  type: 'user type',
  gender: 'M or F',
  age: 0 - 90,
  name: 'a name',
  username: 'user name',
  lastName: 'last name',
  avatar: 'a url to an avatar'
}`


const Card = ({options, isLoading, onRetryClicked, error, title, titleClass, graphClass, ...other}) => {
  return <div {...other}>
    <Typography variant={'title'} className={titleClass}>{title}</Typography>
    <LoadingIndicator show={isLoading} />
    {!status && <ErrorBox message={error && (error.message || 'Failed to contact the server! Make sure its on?')} onRetryClicked={onRetryClicked} />}
    <Chart className={graphClass} options={options} />
  </div>
}



const BinarySearchTree = () => {
  // const inputs = [1, 4, 12, 16, 22, 24, 28, 44, 70]
  const values = [27, 10, 19, 500, 0, 32]
  const tree = {
    value: 27,
    left: {
      value: 14,
      left: {
        value: 10
      },
      right: {
        value: 19
      }
    },
    right: {
      value: 35,
      left: {
        value: 31
      },
      right: {
        value: 42
      }
    }
  }
  const inputs = values.map(value => {
    return { value, tree }
  })
  const findAtTree = (tree = {}, search) => {
    if (!tree.value) {
      return { }
    }
    if (tree.value > search) {
      return findAtTree(tree.left, search)
    } else if (tree.value < search) {
      return findAtTree(tree.right, search)
    }
    return tree
  }
  const onCompare = (input, output) => {
    const result = findAtTree(tree, input.value)
    console.log('comapring', result, output, input)
    return  _.isEqual(result, output)
  }
  const onSuccess = (input, output) => {
    return output
  }
  const { response, error, isLoading, runTests } = withPostRequestTests(inputs, '/assignments/lecture3/binaryTree', onCompare, onSuccess)
  const isCorrect = !error && response !== null
  return (
    <React.Fragment>
      <Typography style={{ position: 'relative' }} variant='p'>
        For the given values: {values.join(', ')}, return the part of the tree where the value is found, otherwise an empty object!<br/>
        <Code>
          {JSON.stringify(tree, null, ' ')}
        </Code>
        Currently the solution is: <Normal style={{ color: isCorrect ? 'green' : 'red' }}>{isCorrect ? 'Correct' : 'Not Correct'}</Normal><br/>
        <LoadingIndicator show={isLoading}/>
      </Typography>
      <Button disabled={isLoading} onClick={runTests}>Retry</Button>
    </React.Fragment>
  )
}

const AveragesWithinGroups = ({ items: input, ...other}) => {
  const [data, setData] = React.useState([])
  const [state, setState] = React.useState({})
  const dispatch = useDispatch()
  const onFetchAverages = () => {
    console.log('fetching averages')
    dispatch({
      [CALL_API]: {
        endpoint: `/assignments/lecture3/users/averages`
      }
    })
      .then((output) => {
        const groups = input.reduce((accumulator, next) => {
          const group = next.type
          return accumulator.includes(group) ? accumulator : [...accumulator, group]
        }, [])

        const grouped = groups.map(group => {
          const items = input.filter(next => next.type === group)
          const length = items.length || Infinity
          const average = items.reduce((sum, next) => sum + next.age, 0) / length
          return {
            name: group,
            value: average
          }
        })
        const theSame = grouped.every((item) => {
          return !!output.find(which => item.name === which.name && item.value === which.value)
        })
        if (!theSame) {
          throw { status: 400, message: 'Your implementation is wrong, check the code and re-run the tests!'}
        }
        return output
      })
      .then((items) => {
        setData(items)
      }, (error) => {
        setState({
          isLoading: false,
          error: error.message || 'Something wrong happened! Check that the server is up and running'
        })
      })
  }
  React.useEffect(() => {
    onFetchAverages()
  }, [input])
  const options = {
    series: [
      {
        data,
        type: 'pie'
      }
    ]
  }
  return <Card options={options} {...state} onRetryClicked={onFetchAverages} title={'Average age between types'} {...other} />
}

const UserTypes = ({ items: input, ...other}) => {
  const [data, setData] = React.useState([])
  const [state, setState] = React.useState({})
  const dispatch = useDispatch()
  const onFetchAverages = () => {
    dispatch({
      [CALL_API]: {
        endpoint: `/assignments/lecture3/users/types`
      }
    })
      .then((output) => {
        const groups = input.reduce((accumulator, next) => {
          const group = next.gender
          return accumulator.includes(group) ? accumulator : [...accumulator, group]
        }, [])
        const grouped = groups.map(group => {
          const length = input.filter(next => next.gender === group).length
          return {
            name: group,
            value: length
          }
        })
        const theSame = grouped.every((item) => {
          return !!output.find(which => item.name === which.name && item.value === which.value)
        })
        console.log('grouped', grouped, output)
        if (!theSame) {
          throw { status: 400, message: 'Your implementation is wrong, check the code and re-run the tests!'}
        }
        return output
      })
      .then((items) => {
        setData(items)
      }, (error) => {
        setState({
          isLoading: false,
          error: error.message || 'Something wrong happened! Check that the server is up and running'
        })
      })
  }
  React.useEffect(() => {
    onFetchAverages()
  }, [input])
  const options = {
    series: [
      {
        data,
        type: 'pie'
      }
    ]
  }
  return <Card options={options} {...state} onRetryClicked={onFetchAverages} title={'Average age between types'} {...other} />
}

const Assignments = (props) => {
  const { classes, section } = props
  const [text, setText] = React.useState('')
  const [items, setItems] = React.useState([])
  const [state, setState] = React.useState({})
  const [edit, setEditing] = React.useState(null)


  const randomUser = () => {
    const gender = faker.name.gender()
    return {
      type: faker.name.jobType(),
      gender: 'M',
      age: faker.random.number({ min: 0, max: 90 }),
      name: faker.name.firstName(gender),
      username: faker.internet.userName(),
      lastName: faker.name.lastName(gender),
      avatar: faker.internet.avatar()
    }
  }
  const editing = edit || randomUser()
  const { isLoading = false, error = '' } = state
  const dispatch = useDispatch()

  React.useEffect(() => {
    setState({ isLoading: true, error: null })
    dispatch({
      [CALL_API]: {
        endpoint: `/assignments/lecture3/users`
      }
    }).then((items) => {
      console.log('items', items)
      setItems(items)
      setState({ isLoading: false, error: null })
    }, (error) => {
      setState({
        isLoading: false,
        error: error.message || 'Something wrong happened! Check that the server is up and running'
      })
    })
  }, [])


  const onSearchChanged = (event) => {
    event.preventDefault()
    setText(event.target.value)
  }

  const onValueChanged = (event) => {
    event.preventDefault()
    const { value, name } = event.target

    setEditing((prevState) => ({
      ...prevState,
      [name]: value
    }))
  }

  const onEditClicked = (item, event) => {
    event.preventDefault()
    setEditing(item)
  }

  const onAddNewClicked = (event) => {
    event.preventDefault()

    setEditing(randomUser())
  }

  const onSaveClicked = (event) => {
    event.preventDefault()
    const isNew = !editing.id
    const method = isNew ? 'POST' : 'PUT'
    const url = isNew ? '/assignments/lecture3/users' : `/assignments/lecture3/users/${editing.id}`
    setState({
      isLoading: true,
      error: null
    })
    dispatch({
      [CALL_API]: {
        endpoint: url,
        options: {
          method: method,
          body: JSON.stringify(editing),
        }
      }
    }).then((item) => {
      setItems((prevItems) => {
        const found = prevItems.find(which => which.id === item.id)
        if (!!found) {
          return prevItems.map(which => {
            if (which.id === item.id) {
              return item
            }
            return which
          })
        }
        return prevItems.concat(item)
      })
      setEditing(undefined)
      setState({
        isLoading: false
      })
    }, (error) => {
      setState({
        isLoading: false,
        error: error.message || 'Something wrong happened! Check that the server is up and running'
      })
    })
  }

  const onCancelClicked = (event) => {
    if (event) {
      event.preventDefault()
    }
    setEditing(undefined)
    setState({ isLoading: false, error: null })
  }

  const onRemoveClicked = (item, event) => {
    event.preventDefault()

    setState({
      isLoading: true,
      error: null
    })
    dispatch({
      [CALL_API]: {
        endpoint: `/assignments/lecture3/users/${item.id}`,
        options: {
          method: 'DELETE',
          body: JSON.stringify(item),
        }
      }
    }).then((item) => {
      setItems((prevItems) => {
        return prevItems.filter(which => which.id !== item.id)
      })
      setEditing(undefined)
      setState({
        isLoading: false
      })
    }, (error) => {
      setState({
        isLoading: false,
        error: error.message || 'Something wrong happened! Check that the server is up and running'
      })
    })
  }
  let graphFunctions = section.children[0]
  let binaryTree = section.children[1]

  const filtered = items.filter(next => {
    const searchText = text.toLowerCase()
    const name = (next.name || '').toLowerCase()
    const username = (next.username || '').toLowerCase()
    const lastName = (next.lastName || '').toLowerCase()
    const type = (next.type || '').toLowerCase()
    return name.includes(searchText) ||
      lastName.includes(searchText) ||
      username.includes(searchText) ||
      type.includes(searchText)
  })

  const cardProps = {
    items,
    titleClass: classes.title,
    className: classes.card,
    graphClass: classes.graph
  }
  return (
    <Fragment>
      <Typography variant={'heading'}>
        Home Assignments
        <Divider />
      </Typography>
      <Typography>
        The following routes and their respective actions and implementations are required to fulfill this home assignment:
        <Code>
          {routes}
        </Code>
      </Typography>
      <Typography id={graphFunctions.id} variant={'title'}>
        {graphFunctions.display}
      </Typography>
      <Typography variant='p'>
        Title: "Implement add/update/delete on a User List"<br />
        We've created the following skeleton and we want to make it functional. Fill in the missing pieces of the code! The requirements are:
        <ol>
          <li>Hold a array of users at the state of this component</li>
          <li>Render rows on the table based on the items of the array</li>
          <li>When edit button is clicked edit that item in the dialog, by passing the model there (TIP: hold the item being edited on the state of this component, when that exists the dialog opens)</li>
          <li>Add the remove functionality</li>
          <li>Based on user types show a graf of the average age of the user</li>
        </ol>
        The user model:
        <Code>
          {model}
        </Code>
      </Typography>
      <Typography variant="p" fontStyle="error">
        {error}
      </Typography>
      <TextField fullWidth margin="normal" onChange={onSearchChanged} value={text} label="Search"/>
      <Button onClick={onAddNewClicked} color="primary">Add New Item</Button>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Avatar</TableCell>
            <TableCell>Name</TableCell>
            <TableCell>Last Name</TableCell>
            <TableCell>Username</TableCell>
            <TableCell>Type (Admin/Normal)</TableCell>
            <TableCell>Age</TableCell>
            <TableCell>Gender</TableCell>
            <TableCell>Action</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {filtered.map((next, index) => {
            return <TableRow key={next.id}>
              <TableCell>
                <img className={classes.avatar}
                     src={next.avatar}
                     alt={next.name}
                />
              </TableCell>
              <TableCell>{next.name}</TableCell>
              <TableCell>{next.lastName}</TableCell>
              <TableCell>{next.username}</TableCell>
              <TableCell>{next.type}</TableCell>
              <TableCell>{next.age}</TableCell>
              <TableCell>{next.gender}</TableCell>
              <TableCell>
                <IconButton onClick={(event) => onEditClicked(next, event)}>
                  <EditIcon />
                </IconButton>
                <IconButton onClick={(event) => onRemoveClicked(next, event)}>
                  <RemoveIcon />
                </IconButton>
              </TableCell>
            </TableRow>
          })}
        </TableBody>
      </Table>
      <Dialog open={!!edit} onClose={onCancelClicked}>
        <DialogTitle>
          My Awesome Dialog - Editing a user!
          <Typography variant="p" fontStyle="error">
            {error}
          </Typography>
        </DialogTitle>
        <DialogContent>
          <TextField fullWidth margin="normal" name="name" onChange={onValueChanged} value={editing.name} label="Name"/>
          <TextField fullWidth margin="normal" name="lastName" onChange={onValueChanged} value={editing.lastName} label="Last Name"/>
          <TextField fullWidth margin="normal" name="username" onChange={onValueChanged} value={editing.username} label="User Name"/>
          <TextField fullWidth margin="normal" name="type" onChange={onValueChanged} select value={editing.type} label="Select Type">
            <ListItem value={editing.type}>{editing.type}</ListItem>
          </TextField>
          <TextField fullWidth margin="normal" name="age" onChange={onValueChanged} value={editing.age} label="Age"/>
          <RadioGroup name="gender" onChange={onValueChanged} value={editing.gender}>
            <FormControlLabel value="M" control={<Radio />} label="Male"/>
            <FormControlLabel value="F" control={<Radio />} label="Female"/>
          </RadioGroup>
        </DialogContent>
        <DialogActions>
          <Button onClick={onCancelClicked} color="secondary">
            Cancel
          </Button>
          <Button onClick={onSaveClicked} color="primary">
            Save
          </Button>
        </DialogActions>
      </Dialog>
      <div className={classes.graphs}>
        <AveragesWithinGroups {...cardProps} title={'Average age between types'} />
        <UserTypes {...cardProps} title={'Males vs Females between types'} />
      </div>
      <Typography id={binaryTree.id} variant={'title'}>
        {binaryTree.display}
      </Typography>
      <Typography variant='p'>
        Title: "Implement the Binary Search Tree"<br />
        Description: "Using Binary Search Tree I will search for the given value at the given tree structure"<br />
        To understand how binary search tree works visit: <SimpleLink href="https://www.tutorialspoint.com/data_structures_algorithms/binary_search_tree.htm">Binary Search Tree Explanation</SimpleLink><br />
      </Typography>
      <Typography variant='p'>
        Implement the algorithm that searches the given tree and returns the node where the value exists:
      </Typography>
      <img src={BinarySearchTreeImage} />
      <BinarySearchTree/>

      <LoadingIndicator show={isLoading}/>
    </Fragment>
  )
}

export default withStyles(styles)(Assignments)
