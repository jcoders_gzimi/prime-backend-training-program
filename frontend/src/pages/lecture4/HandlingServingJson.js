/**
 * Created by LeutrimNeziri on 09/04/2019.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import SimpleLink from "presentations/rows/SimpleLink";
import { Bold, Highlighted } from "presentations/Label";
import RouteErrorsPreview from 'assets/images/lecture4/route-errors-preview.png'
import Code from "../../presentations/Code";

const styles = ({ typography }) => ({
  root: {},
})

const simpleClass = `// Note: can use getters/setters as well; here we just use public fields directly.
// if using getters/setters, you can keep the fields \`protected\` or \`private\`
public static class Person {
  public String firstName;
  public String lastName;
  public int age;
}`

const parsingExample = `Person person = new Person();
person.firstName = "Foo";
person.lastName = "Bar";
person.age = 30;
JsonNode personJson = Json.toJson(person); // {"firstName": "Foo", "lastName": "Bar", "age": 30}`

const parseToOBject = `// parse the JSON as a JsonNode
JsonNode json = Json.parse("{\\"firstName\\":\\"Foo\\", \\"lastName\\":\\"Bar\\", \\"age\\":13}");
// read the JsonNode as a Person
Person person = Json.fromJson(json, Person.class);`

const anyContentExample = `public Result sayHello(Http.Request request) {
  JsonNode json = request.body().asJson();
  if (json == null) {
    return badRequest("Expecting Json data");
  } else {
    String name = json.findPath("name").textValue();
    if (name == null) {
      return badRequest("Missing parameter [name]");
    } else {
      return ok("Hello " + name);
    }
  }
}`
const anyContentToClassExample = `public Result sayHello(Http.Request request) {
  Optional<Person> person = request.body().parseJson(Person.class);
  return person.map(p -> ok("Hello, " + p.firstName)).orElse(badRequest("Expecting Json data"));
}`

const explicitlySpecify = `@BodyParser.Of(BodyParser.Json.class)
public Result sayHello(Http.Request request) {
  JsonNode json = request.body().asJson();
  String name = json.findPath("name").textValue();
  if (name == null) {
    return badRequest("Missing parameter [name]");
  } else {
    return ok("Hello " + name);
  }
}`

const jsonNodeResponse = `public Result sayHello() {
  ObjectNode result = Json.newObject();
  result.put("exampleField1", "foobar");
  result.put("exampleField2", "Hello world!");
  return ok(result);
}`
const javaToJsonNode = `public Result getPeople() {
  List<Person> people = personDao.findAll();
  return ok(Json.toJson(people));
}`

const disableDefaultObjectMapper = `play.modules.disabled += "play.core.ObjectMapperModule"`

const serializationFeature = `akka.serialization.jackson.play.serialization-features.WRITE_NUMBERS_AS_STRINGS=true`

const customObjectMapperProvider = `public class JavaJsonCustomObjectMapper implements Provider<ObjectMapper> {

  @Override
  public ObjectMapper get() {
    ObjectMapper mapper =
        new ObjectMapper()
            // enable features and customize the object mapper here ...
            .enable(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS)
            .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

    // Needs to set to Json helper
    Json.setObjectMapper(mapper);

    return mapper;
  }
}`

const bindItToHelper = `public class JavaJsonCustomObjectMapperModule extends AbstractModule {

  @Override
  protected void configure() {
    bind(ObjectMapper.class).toProvider(JavaJsonCustomObjectMapper.class).asEagerSingleton();
  }
}`

const enableCutomModule = `play.modules.enabled += "path.to.JavaJsonCustomObjectMapperModule"`

class HandlingServingJson extends React.Component {
  render() {
    const { classes, section } = this.props
    const mappingJavaToJSON = section.children[0]
    const handlingJSONRequest = section.children[1]
    const servingJsonResponse = section.children[2]
    const advancedUsage = section.children[3]
    return (
      <Fragment>
        <Typography variant={'heading'}>
          Section 4: {section.display}
          <Typography>
            Resources: <SimpleLink href="https://www.playframework.com/documentation/2.8.x/JavaJsonActions">https://www.playframework.com/documentation/2.8.x/JavaJsonActions</SimpleLink>
          </Typography>
          <Divider />
        </Typography>
        <Typography>
          In Java, Play uses the <SimpleLink href="https://github.com/FasterXML/jackson">Jackson</SimpleLink> JSON library to convert objects to and from JSON. Play’s actions work with the JsonNode type and the framework provides utility methods for conversion in the play.libs.Json API.
        </Typography>
        <Typography id={mappingJavaToJSON.id} variant={'title'}>
          {mappingJavaToJSON.display}
        </Typography>
        <Typography>
          Jackson allows you to easily convert Java objects to JSON by looking at field names, getters and setters. As an example we’ll use the following simple Java object:
          <Code>
            {simpleClass}
          </Code>
          We can parse the JSON representation of the object and create a new Person:
          <Code>
            {parsingExample}
          </Code>
          Similarly, we can write the Person object to a JsonNode:
          <Code>
            {parseToOBject}
          </Code>
        </Typography>
        <Typography id={handlingJSONRequest.id} variant={'title'}>
          {handlingJSONRequest.display}
        </Typography>
        <Typography>
          A JSON request is an HTTP request using a valid JSON payload as request body. Its <Highlighted>Content-Type</Highlighted> header must specify the <Highlighted>text/json</Highlighted> or <Highlighted>application/json</Highlighted> MIME type.
        </Typography>
        <Typography>
          By default an action uses an any content body parser, which you can use to retrieve the body as JSON (actually as a Jackson JsonNode):
          <Code>
            {anyContentExample}
          </Code>
          <Code>
            {anyContentToClassExample}
          </Code>
          Of course it’s way better (and simpler) to specify our own <Highlighted>BodyParser</Highlighted> to ask Play to parse the content body directly as JSON:
          <Code>
            {explicitlySpecify}
          </Code>
        </Typography>
        <Typography id={servingJsonResponse.id} variant={'title'}>
          {servingJsonResponse.display}
        </Typography>
        <Typography>
          In our previous example we handled a JSON request, but replied with a <Highlighted>text/plain</Highlighted> response. Let’s change that to send back a valid JSON HTTP response:
          <Code>
            {jsonNodeResponse}
          </Code>
          You can also return a Java object and have it automatically serialized to JSON by the Jackson library:
          <Code>
            {javaToJsonNode}
          </Code>
        </Typography>
        <Typography id={advancedUsage.id} variant={'title'}>
          {advancedUsage.display}
        </Typography>

        <Typography>
          There are two possible ways to customize the ObjectMapper for your application.
        </Typography>
        <Typography variant="section">
          Configurations in application.conf
        </Typography>
        <Typography>
          Because Play uses Akka Jackson serialization support, you can configure the ObjectMapper based on your application needs. The <SimpleLink href="https://github.com/FasterXML/jackson-databind/wiki/JacksonFeatures">documentation for jackson-databind</SimpleLink> Features explains how you can further customize JSON conversion process, including Mapper, Serialization and Deserialization features.
        </Typography>
        <Typography>
          If you would like to use Play’s Json APIs (toJson/fromJson) with a customized ObjectMapper, you need to add the custom configurations in your application.conf. For example, if you want to set a serialization configuration:
          <Code>
            {serializationFeature}
          </Code>
        </Typography>

        <Typography variant="section">
          Custom binding for ObjectMapper
        </Typography>
        <Typography>
          If you still want to take completely over the <Highlighted>ObjectMapper</Highlighted> creation, this is possible by overriding its binding configuration. You first need to disable the default <Highlighted>ObjectMapper</Highlighted> module in your <Highlighted>application.conf</Highlighted>
          <Code>
            {disableDefaultObjectMapper}
          </Code>
          Then you can create a provider for your <Highlighted>ObjectMapper</Highlighted>:
          <Code>
            {customObjectMapperProvider}
          </Code>
          And bind it via Guice as an eager singleton so that the <Highlighted>ObjectMapper</Highlighted> will be set into the <Highlighted>Json</Highlighted> helper:
          <Code>
            {bindItToHelper}
          </Code>
          Afterwards enable the Module:
          <Code>
            {enableCutomModule}
          </Code>
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(HandlingServingJson)
