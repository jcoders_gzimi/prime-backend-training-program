/**
 * Created by Agon Lohaj on 27/08/2020.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import SimpleLink from "presentations/rows/SimpleLink";
import { Bold, Highlighted } from "presentations/Label";
import Code from "presentations/Code";
import Table from "@material-ui/core/Table/Table";
import { Button, IconButton, TableBody, TableCell, TableHead, TableRow } from "@material-ui/core";
import EditIcon from "@material-ui/core/SvgIcon/SvgIcon";

const styles = ({ typography }) => ({
  root: {},
})

const dependencies = `libraryDependencies ++= Seq(
  ehcache,
  ...
  "com.github.karelcemus" %% "play-redis" % "2.5.0"
)`

const applicationConf = `play.cache.redis {
  # redis host
  host:       127.0.0.1

  # redis server: port
  port:       6379
  # redis server: database number (optional)
  database:   4
  # authentication password (optional)
  password:   null #"redis_rocks!"

  # do not bind default unqualified APIs
  bind-default: false

  # name of the instance in simple configuration,
  # i.e., not located under \`instances\` key
  # but directly under 'play.cache.redis'
  default-cache: "redis"
}`

const namedCache = `@Inject
@NamedCache("redis")
AsyncCacheApi cacheApi;

public CompletionStage<Result> cachedRedis (Http.Request request) {
  return cacheApi.getOrElseUpdate(
    "redis-expiry", // the key
    () -> CompletableFuture.supplyAsync(() -> "Some expensive value"), // if it doesn't exist how to update it
    20 // expiration time
  )
  .thenApply(Results::ok);
}`

const boundCaches = `play.cache.bindCaches = ["training"]
play.cache.createBoundCaches = false`

const ehCacheConfig = `<ehcache xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" updateCheck="false">

    <!-- This is a default configuration for 256Mb of cached data using the JVM's heap, but it must be adjusted
         according to specific requirement and heap sizes -->

    <cache
        name="training"
        maxBytesLocalHeap="256000000"
        eternal="false"
        timeToIdleSeconds="120"
        timeToLiveSeconds="120"
        overflowToDisk="false"
        maxElementsOnDisk="10000000"
        diskPersistent="false"
        diskExpiryThreadIntervalSeconds="120"
        memoryStoreEvictionPolicy="LRU"
    />

</ehcache>`

const bindings = `@Inject
@NamedCache("redis")
AsyncCacheApi cacheApi; // I'm dynamically bound to a Redis Cache

@Inject
@NamedCache("training")
AsyncCacheApi ehCacheApi; // I'm dynamically bound to a default EH Cache`

const twoTypesCaches = `@Inject
@NamedCache("redis")
AsyncCacheApi cacheApi; // Async, lazy caching

@Inject
@NamedCache("redis")
SyncCacheApi syncCacheApi; // Synchronous caching`

const synchCacheApi = `public interface SyncCacheApi {
  /**
   * Retrieves an object by key.
   *
   * @param <T> the type of the stored object
   * @param key the key to look up
   * @return the object or null
   * @deprecated Deprecated as of 2.7.0. Use {@link #getOptional(String)} instead.
   */
  @Deprecated
  <T> T get(String key);

  /**
   * Retrieves an object by key.
   *
   * @param <T> the type of the stored object
   * @param key the key to look up
   * @return the object wrapped in an Optional
   */
  <T> Optional<T> getOptional(String key);

  /**
   * Retrieve a value from the cache, or set it from a default Callable function.
   *
   * @param <T> the type of the value
   * @param key Item key.
   * @param block block returning value to set if key does not exist
   * @param expiration expiration period in seconds.
   * @return the value
   */
  <T> T getOrElseUpdate(String key, Callable<T> block, int expiration);

  /**
   * Retrieve a value from the cache, or set it from a default Callable function.
   *
   * <p>The value has no expiration.
   *
   * @param <T> the type of the value
   * @param key Item key.
   * @param block block returning value to set if key does not exist
   * @return the value
   */
  <T> T getOrElseUpdate(String key, Callable<T> block);

  /**
   * Sets a value with expiration.
   *
   * @param key Item key.
   * @param value The value to set.
   * @param expiration expiration in seconds
   */
  void set(String key, Object value, int expiration);

  /**
   * Sets a value without expiration.
   *
   * @param key Item key.
   * @param value The value to set.
   */
  void set(String key, Object value);

  /**
   * Removes a value from the cache.
   *
   * @param key The key to remove the value for.
   */
  void remove(String key);
}`

const asyncCacheApi = `AsyncCacheApi {

  /** @return a synchronous version of this cache, which can be used to make synchronous calls. */
  default SyncCacheApi sync() {
    return new DefaultSyncCacheApi(this);
  }

  /**
   * Retrieves an object by key.
   *
   * @param <T> the type of the stored object
   * @param key the key to look up
   * @return a CompletionStage containing the value
   * @deprecated Deprecated as of 2.7.0. Use {@link #getOptional(String)} instead.
   */
  @Deprecated
  <T> CompletionStage<T> get(String key);

  /**
   * Retrieves an object by key.
   *
   * @param <T> the type of the stored object
   * @param key the key to look up
   * @return a CompletionStage containing the value wrapped in an Optional
   */
  <T> CompletionStage<Optional<T>> getOptional(String key);

  /**
   * Retrieve a value from the cache, or set it from a default Callable function.
   *
   * @param <T> the type of the value
   * @param key Item key.
   * @param block block returning value to set if key does not exist
   * @param expiration expiration period in seconds.
   * @return a CompletionStage containing the value
   */
  <T> CompletionStage<T> getOrElseUpdate(
      String key, Callable<CompletionStage<T>> block, int expiration);

  /**
   * Retrieve a value from the cache, or set it from a default Callable function.
   *
   * <p>The value has no expiration.
   *
   * @param <T> the type of the value
   * @param key Item key.
   * @param block block returning value to set if key does not exist
   * @return a CompletionStage containing the value
   */
  <T> CompletionStage<T> getOrElseUpdate(String key, Callable<CompletionStage<T>> block);

  /**
   * Sets a value with expiration.
   *
   * @param key Item key.
   * @param value The value to set.
   * @param expiration expiration in seconds
   * @return a CompletionStage containing the value
   */
  CompletionStage<Done> set(String key, Object value, int expiration);

  /**
   * Sets a value without expiration.
   *
   * @param key Item key.
   * @param value The value to set.
   * @return a CompletionStage containing the value
   */
  CompletionStage<Done> set(String key, Object value);

  /**
   * Removes a value from the cache.
   *
   * @param key The key to remove the value for.
   * @return a CompletionStage containing the value
   */
  CompletionStage<Done> remove(String key);

  /**
   * Removes all values from the cache. This may be useful as an admin user operation if it is
   * supported by your cache.
   *
   * @throws UnsupportedOperationException if this cache implementation does not support removing
   *     all values.
   * @return a CompletionStage containing either a Done when successful or an exception when
   *     unsuccessful.
   */
  CompletionStage<Done> removeAll();
}`

class RedisCache extends React.Component {
  render() {
    const { classes, section } = this.props
    const providedApis = section.children[0]
    // const noneBlockingActions = section.children[1]


    const traits = [
      ['1.','play.api.cache.redis.CacheApi', 'Scala', 'blocking', 'advanced'],
      ['2.','play.api.cache.redis.CacheAsyncApi', 'Scala', 'none-blocking', 'advanced'],
      ['3.','play.cache.redis.AsyncCacheApi', 'Java', 'blocking', 'advanced'],
      ['4.','play.api.cache.SyncCacheApi', 'Scala', 'blocking', 'basic'],
      ['5.','play.api.cache.AsyncCacheApi', 'Scala', 'none-blocking', 'basic'],
      ['6.','play.cache.SyncCacheApi', 'Java', 'blocking', 'basic'],
      ['7.','play.cache.AsyncCacheApi', 'Java', 'none-blocking', 'basic'],
      ['7.','play.cache.AsyncCacheApi', 'Java', 'none-blocking', 'basic']
    ]
    return (
      <Fragment>
        <Typography variant={'heading'}>
          Section 6: {section.display}
          <Typography>
            Resources: <ol>
            <li><SimpleLink href="https://github.com/KarelCemus/play-redis">https://github.com/KarelCemus/play-redis</SimpleLink></li>
          </ol>
          </Typography>
          <Divider />
        </Typography>
        <Typography>
          In this section we are going to explore on how easely we can plugin Redis as our caching service using a Play Module build by the community called: <Highlighted>Play Redis</Highlighted>
        </Typography>
        <Typography>
          To get started first we need to download and install redis via: <SimpleLink href="https://redis.io/">https://redis.io/</SimpleLink>. Download the latest version and have it running.
        </Typography>
        <Typography>
          In our Play project Redis is already setup by doing:
          <Code>
            {dependencies}
          </Code>
          And also at our reference.conf:
          <Code>
            {applicationConf}
          </Code>
          And a sample usage:
          <Code>
            {namedCache}
          </Code>
          In order to make redis custom binding work together with ehcache we had to do the following:
          <Code>
            {boundCaches}
          </Code>
          All the rest of the config resides at <Highlighted>ehcache.xml</Highlighted>:
          <Code>
            {ehCacheConfig}
          </Code>
          So that means that we have two types of caching configured:
          <Code>
            {bindings}
          </Code>

          Play offers two default bindings for each of the <Highlighted>@Named</Highlighted> caches:
          <Code>
            {twoTypesCaches}
          </Code>
          Using them is as simple as calling their interfaces:
          <Code>
            {synchCacheApi}
          </Code>
          Or Async:
          <Code>
            {asyncCacheApi}
          </Code>
          Besides the compatibility with all Play's cache APIs, Redis Cache API introduces more evolved API providing lots of handful operations. Besides the basic methods such as get, set and remove, it provides more convenient methods such as expire, exists, invalidate and much more.
        </Typography>
        <Table>

        </Table>
        <Typography>
          The implementation builds on the top of Akka actor system, it is completely non-blocking and asynchronous under the hood, though it also provides blocking APIs to ease the use.
        </Typography>
        <Typography id={providedApis.id} variant={'title'}>
          {providedApis.display}
        </Typography>
        <Typography>
          This library delivers a single module with following implementations of the API. While the core of the framework is <Bold>fully non-blocking</Bold>, most of the provided facades are only <Bold>blocking wrappers</Bold>.
        </Typography>

        <Table>
          <TableHead>
            <TableRow>
              <TableCell> </TableCell>
              <TableCell>Traits</TableCell>
              <TableCell>Language</TableCell>
              <TableCell>Blocking</TableCell>
              <TableCell>Features</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {traits.map((next, index) => {
              return (
                <TableRow key={`row-${index}`}>
                  {next.map((item, cell) => (
                    <TableCell key={`cell-${cell}`}>
                      {item}
                    </TableCell>
                  ))}
                </TableRow>
            )})}
          </TableBody>
        </Table>
      </Fragment>
    )
  }
}

export default withStyles(styles)(RedisCache)
