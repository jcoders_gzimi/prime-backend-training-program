/**
 * Created by Agon Lohaj on 27/08/2020.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import SimpleLink from "presentations/rows/SimpleLink";
import { Bold, Highlighted, Italic } from "presentations/Label";
import Code from "presentations/Code";
import Button from '@material-ui/core/Button'
import { useDispatch } from 'react-redux'
import { CALL_API } from "../../middleware/Api";

const styles = ({ size, typography }) => ({
  root: {},
  section: { }
})

const carClass = `public class Car {
	private String manufacturer;
	private String licensePlate;
	private int seatCount;
	private boolean upAndRunning;
	private List<String> tags = new ArrayList<>();
}`

const annotatedCarClass = `public class Car {
	// Field level constraint
	@NotEmpty(message = "Cannot be empty!")
	private String manufacturer;

	@NotEmpty(message = "License plate cannot be empty!")
	@Size(min = 2, max = 14, message = "The length should be between 2 and 14")
	private String licensePlate;

	@Min(value = 2, message = "Cannot be lower than 2")
	@Max(value = 100, message = "Cannot be higher than 100")
	private int seatCount;

	@AssertTrue(message = "The car must be up and running in order to work!")
	private boolean upAndRunning;

	private List<String> tags = new ArrayList<>();
	
	// Accessor or Property level contraint
	@Size(min = 1, message = "There should be at least one available!")
	public List<String> getTags() {
		return tags;
	}
}`

const fieldLevelConstraint = `@NotEmpty(message = "Cannot be empty!")
private String manufacturer;`

const methodLevelConstraint = `private List<String> tags = new ArrayList<>();

@Size(min = 1, message = "There should be at least one available!")
public List<String> getTags() {
  return tags;
}`

const classLevelConstraing = `@PassengerCount
public class Car {
...
}`

const rentalLevelContraint = `public class Rental extends Car {
	@NotEmpty(message = "Cannot be empty!")
	private String rental;
}`

const javaClassValidation = `@BodyParser.Of(BodyParser.Json.class)
public Result save (Http.Request request) {
  JsonNode node = request.body().asJson();

  Car car = Json.fromJson(node, Car.class);
  if (Strings.isNullOrEmpty(car.getManufacturer())) {
    return badRequest("Manufacturer cannot be empty!");
  }
  if (Strings.isNullOrEmpty(car.getLicensePlate())) {
    return badRequest("License Place cannot be empty!");
  }
  if (car.getLicensePlate().length() < 2 || car.getLicensePlate().length() > 14) {
    return badRequest("License Place should be between 2 and 14!");
  }
  if (car.getSeatCount() < 2 || car.getSeatCount() > 100) {
    return badRequest("Seat count cannot be less than 2!");
  }
  if (!car.isUpAndRunning()) {
    return badRequest("The car should be up and running!");
  }
  if (car.getTags().size() == 0) {
    return badRequest("There should be at least 1 tag!");
  }
  return ok(Json.toJson(car));
}`

const usingAnnotations = `public Result save (Http.Request request) {
  JsonNode node = request.body().asJson();

  Car car = Json.fromJson(node, Car.class);
  String errors = HibernateValidator.validate(car);
  if (!Strings.isNullOrEmpty(errors)) {
    return badRequest(errors);
  }
  return ok(Json.toJson(car));
}`

const hibernateHelperMethods = `public class HibernateValidator {

	public static <T> String validate(T t) {
		Set<ConstraintViolation<T>> errors = HibernateValidator.apply(t);
		if (errors.size() == 0) {
			return "";
		}
		return HibernateValidator.formatErrors(errors);
	}

	private static <T> Set<ConstraintViolation<T>> apply(T t) {
		Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
		return validator.validate(t);
	}

	private static <T> String formatErrors (Set<ConstraintViolation<T>> errors) {
		return errors.stream()
				.map((error) -> String.format("%s %s", Strings.capitalize(error.getPropertyPath().toString()), error.getMessage()))
				.collect(Collectors.joining(", "));
	}
}`

const casingEnum = `public enum CaseMode {
  UPPER, 
  LOWER;
}`

const annotation = `import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target( { METHOD, FIELD, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = CheckCaseValidator.class)
@Documented
public @interface CheckCase {

    String message() default "{com.mycompany.constraints.checkcase}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
    
    CaseMode value();

}`


const severity = `public class Severity {
    public static class Info extends Payload {};
    public static class Error extends Payload {};
}

public class ContactDetails {
    @NotNull(message="Name is mandatory", payload=Severity.Error.class)
    private String name;

    @NotNull(message="Phone number not specified, but not mandatory", payload=Severity.Info.class)
    private String phoneNumber;

    // ...
}`

const implementation = `import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CheckCaseValidator implements ConstraintValidator<CheckCase, String> {
  private CaseMode caseMode;
	private String message;

	public void initialize (CheckCase constraintAnnotation) {
		this.caseMode = constraintAnnotation.value();
		this.message = constraintAnnotation.message();
	}

	public boolean isValid (String object, ConstraintValidatorContext constraintContext) {
		if (object == null)
			return true;

		boolean isValid;
		if (caseMode == CaseMode.UPPER) {
			isValid = object.equals(object.toUpperCase());
		} else {
			isValid = object.equals(object.toLowerCase());
		}

		if (!isValid) {
			constraintContext.disableDefaultConstraintViolation();
			constraintContext.buildConstraintViolationWithTemplate(message).addConstraintViolation();
		}
		return isValid;
	}
}`

const customAnnotationUsage = `public class Car {
	@NotEmpty(message = "Cannot be empty!")
	private String manufacturer;

	@NotEmpty(message = "License plate cannot be empty!")
	@Size(min = 2, max = 14, message = "The length should be between 2 and 14")
	@CheckCase(value = CaseMode.UPPER)
	private String licensePlate;

	@Min(value = 2, message = "Cannot be lower than 2")
	@Max(value = 100, message = "Cannot be higher than 100")
	private int seatCount;

	@AssertTrue(message = "The car must be up and running in order to work!")
	private boolean upAndRunning;

	private List<String> tags = new ArrayList<>();

	@Size(min = 1, message = "There should be at least one available!")
	public List<String> getTags() {
		return tags;
	}
}`

const compositionAnnotation = `import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@NotNull
@Size(min = 2, max = 14)
@CheckCase(CaseMode.UPPER)
@Target( { METHOD, FIELD, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = {})
@Documented
public @interface ValidLicensePlate {

    String message() default "{com.mycompany.constraints.validlicenseplate}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}`

const oneConstraintToRuleThemAll = `public class Car {

    @ValidLicensePlate
    private String licensePlate;

    //...

}`

const DataValidation = (props) => {
  const { classes, section } = props
  const dataQuality = section.children[0]
  const usingJava = section.children[1]
  const usingHibernate = section.children[2]
  const customConstraint = section.children[3]
  const composition = section.children[4]

  return (
    <Fragment>
      <Typography variant={'heading'}>
        Section 6: {section.display}
        <Typography>
          Resources: <ol>
          <li><SimpleLink href="https://docs.jboss.org/hibernate/validator/4.3/reference/en-US/html_single/">https://docs.jboss.org/hibernate/validator/4.3/reference/en-US/html_single/</SimpleLink></li>
          <li><SimpleLink href="https://hibernate.org/validator/documentation/getting-started/">https://hibernate.org/validator/documentation/getting-started/</SimpleLink></li>
        </ol>

        </Typography>
        <Divider />
      </Typography>
      <Typography id={dataQuality.id} variant={'title'}>
        {dataQuality.display}
      </Typography>
      <Typography>
        Most of the time when dealing with data storage, handling user requests and providing services in general via a Backend API it is required that we do some data validation and verification in order to ensure data quality. Not only that we use different validation and verification techniques to also test whether or not an API meets the needs of the client it servers but also whether it complies within a regulation, requirement, specification, or imposed condition.
      </Typography>
      <Typography>
        As an example if you wish to get subscribed to receive email notifications every time a change occurs, it is required that:
        <ol>
          <li>You provide a valid email where you can receive the notifications</li>
          <li>Emails are sent regardless of server workload, traffic, change size within 5 min after they occur (none functional requirement)</li>
        </ol>
        So, in order for the service provider to fulfill its promise certain criterias and conditions have to be fulfilled and the API server has to meet certain constraints/regulations.
      </Typography>
      <Typography>
        Also, due to the fact that the digital world is increasing in data size (data abundance) companies rely on data quality to have meaningful insights that can empower them to be data driven!
      </Typography>
      <Typography>
        This is the motivation behind ensuring data quality through validation and verification and this is a crucial part of an API server that provides services to its clients. Any time a service cannot be fulfilled it is important that the right reason (status code) as well as message is provided back to the client for transparency and also to manage expectations!
      </Typography>
      <Typography>
        The situation which we definitely want to avoid at all cost is on providing false positives (I can do it, but you don't deliver) or false negatives (I failed, but really I still caused change).
      </Typography>
      <Typography id={usingJava.id} variant={'title'}>
        {usingJava.display}
      </Typography>
      <Typography>
        The simplest way to go around this is to use plain Java for which there is nothing wrong with it! Let us take an example and go through with it. Given the following class:
        <Code>
          {carClass}
        </Code>
        This is how you would normally validate a <Highlighted>Car</Highlighted> object before inserting it:
        <Code>
          {javaClassValidation}
        </Code>
      </Typography>
      <Typography id={usingHibernate.id} variant={'title'}>
        {usingHibernate.display}
      </Typography>
      <Typography>
        What if there is a much easier way on doing the above validation, but doing that using Annotations. Our <Highlighted>Car</Highlighted> class will now look like this:
        <Code>
          {annotatedCarClass}
        </Code>
        And than our code goes to:
        <Code>
          {usingAnnotations}
        </Code>
        This way, we are keeping the code clean, and focusing on validation where we are modeling (at the Car class). This way we can also reuse and easely introduce validation at a central place, rather than looking at all the places where validation is necessary!
      </Typography>
      <Typography>
        By doing so, we have ensured that the data that meets the constraints is the one that always gets to be processed!
      </Typography>
      <Typography>
        Using Hibernate, we can define Constraints on:
        <ol>
          <li>
            <Bold>Field Level</Bold>
            <Code>
              {fieldLevelConstraint}
            </Code>
          </li>
          <li>
            <Bold>Method Level</Bold>
            <Code>
              {methodLevelConstraint}
            </Code>
          </li>
          <li>
            <Bold>Class Level</Bold>
            <Code>
              {classLevelConstraing}
            </Code>
          </li>
          <li>
            <Bold>Inherited</Bold>
            <Code>
              {rentalLevelContraint}
            </Code>
          </li>
        </ol>
        In order to retreive the errors formatted we are going to be using this helper class:
        <Code>
          {hibernateHelperMethods}
        </Code>
      </Typography>
      <Typography variant="section">
        Build in Constraints
      </Typography>
      <Typography>
        Follow this link to learn more about the build in constraints: <SimpleLink href="https://docs.jboss.org/hibernate/validator/4.3/reference/en-US/html_single/#validator-defineconstraints-spec">https://docs.jboss.org/hibernate/validator/4.3/reference/en-US/html_single/#validator-defineconstraints-spec</SimpleLink>
      </Typography>

      <Typography id={customConstraint.id} variant={'title'}>
        {customConstraint.display}
      </Typography>
      <Typography>
        In order to build your own validation, the following three steps are required:
        <ol>
          <li>Create a constraint annotation</li>
          <li>Implement a validator</li>
          <li>Define a default error message</li>
        </ol>
      </Typography>
      <Typography variant="section">
        Constraint Annotation
      </Typography>
      <Typography>
        Let's write a constraint annotation, that can be used to express that a given string shall either be upper case or lower case.
      </Typography>
      <Typography>
        First we need a way to express the two case modes. We might use String constants, but a better way to go is to use a Java 5 enum for that purpose:
        <Code>
          {casingEnum}
        </Code>
        Now we can define the actual constraint annotation. If you've never designed an annotation before, this may look a bit scary, but actually it's not that hard:
        <Code>
          {annotation}
        </Code>
        An annotation type is defined using the @interface keyword. All attributes of an annotation type are declared in a method-like manner. The specification of the Bean Validation API demands, that any constraint annotation defines
        <ol>
          <li>an attribute message that returns the default key for creating error messages in case the constraint is violated</li>
          <li>an attribute groups that allows the specification of validation groups, to which this constraint belongs. This must default to an empty array of type {`Class<?>`}.</li>
          <li>an attribute payload that can be used by clients of the Bean Validation API to assign custom payload objects to a constraint. This attribute is not used by the API itself.</li>
        </ol>
        An example for a custom payload could be the definition of a severity.
        <Code>
          {severity}
        </Code>
      </Typography>
      <Typography>
        In addition we annotate the annotation type with a couple of so-called meta annotations:
        <ol>
          <li>{`@Target({ METHOD, FIELD, ANNOTATION_TYPE })`}: Says, that methods, fields and annotation declarations may be annotated with @CheckCase (but not type declarations e.g.)</li>
          <li>@Retention(RUNTIME): Specifies, that annotations of this type will be available at runtime by the means of reflection</li>
          <li>@Constraint(validatedBy = CheckCaseValidator.class): Specifies the validator to be used to validate elements annotated with @CheckCase</li>
          <li>@Documented: Says, that the use of @CheckCase will be contained in the JavaDoc of elements annotated with it</li>
        </ol>
        Next, we need to implement a constraint validator, that's able to validate elements with a @CheckCase annotation. To do so, we implement the interface ConstraintValidator as shown below:
        <Code>
          {implementation}
        </Code>
        And usage:
        <Code>
          {customAnnotationUsage}
        </Code>
      </Typography>
      <Typography id={composition.id} variant={'title'}>
        {composition.display}
      </Typography>
      <Typography>
        Looking at the licensePlate field of the <Italic>Car</Italic> class, we see three constraint annotations already. In complexer scenarios, where even more constraints could be applied to one element, this might become a bit confusing easily. Furthermore, if we had a licensePlate field in another class, we would have to copy all constraint declarations to the other class as well, violating the DRY (Don't repeat yourself) principle.

      </Typography>
      <Typography>
        This problem can be tackled using compound constraints. In the following we create a new constraint annotation @ValidLicensePlate, that comprises the constraints @NotNull, @Size and @CheckCase:
        <Code>
          {compositionAnnotation}
        </Code>
        To do so, we just have to annotate the constraint declaration with its comprising constraints (btw. that's exactly why we allowed annotation types as target for the @CheckCase annotation). As no additional validation is required for the @ValidLicensePlate annotation itself, we don't declare a validator within the @Constraint meta annotation.
      </Typography>
      <Typography>
        Using the new compound constraint at the licensePlate field now is fully equivalent to the previous version, where we declared the three constraints directly at the field itself:
        <Code>
          {oneConstraintToRuleThemAll}
        </Code>
      </Typography>
    </Fragment>
  )
}

export default withStyles(styles)(DataValidation)
