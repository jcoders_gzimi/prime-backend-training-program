/**
 * Created by Agon Lohaj on 08/03/2020.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
const styles = ({ typography }) => ({
  root: {},
})

class PlanProgram extends React.Component {
  render() {
    const { classes } = this.props
    return (
      <Fragment>
        <Typography variant={'heading'}>
          Plan Program
          <Divider />
        </Typography>
        <Typography variant='p'>
          <ol>
            <li>
              Introduction to the course, get to know each other, overview of the program, expectations, project setup and account setups and <br/>
              Your first ever commit, making a Pull request and the importance of it, getting all the tasks setup for the rest of the training
            </li>
            <li>
              Java recap, OOP principles and <br/>
              Java new features (Part 1)
            </li>
            <li>
              Java new features (Part 2) and <br/>
              Introduction to Play Framework and REST API
            </li>
            <li>
              Routing, Manipulating Requests and <br/>
              Body Parsers, Handling and serving JSON
            </li>
            <li>
              Action Composition, Handling file uploads and <br/>
              Handling Asyncronous Results
            </li>
            <li>
              Validation, Dependency Injection and <br/>
              Redis Caching
            </li>
            <li>
              Dependency Injection (Part 2) and <br/>
              Introduction to Akka and Akka actors
            </li>
            <li>
              Akka Cluster and <br/>
              Akka Pub/Sub with Web Socket
            </li>
            <li>
              Akka Singleton and <br/>
              Akka Streaming
            </li>
            <li>
              Introduction to Mongo <br/>
              Simple Queries
            </li>
            <li>
              CRUD operations and <br/>
              Aggregation Pipeline
            </li>
            <li>
              More Aggregations and <br/>
              Advanced usages with Mongo
            </li>
            <li>
              Documentation, Testing and CD/CI
            </li>
            <li>
              Recap and <br/>
              Wrap UP!
            </li>
          </ol>
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(PlanProgram)
