/**
 * Created by Agon Lohaj on 09/04/2019.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import { PAGES } from 'Constants';
import Intro from "pages/lecture5/Intro";
import PlayAsync from "pages/lecture5/PlayAsync";
import FileUploads from "pages/lecture5/FileUploads";
import ActionComposition from "pages/lecture5/ActionComposition";
import React from "react";

const styles = () => ({
  root: {},
})

class Index extends React.Component {
  render() {
    const { breadcrumbs } = this.props

    let section = breadcrumbs[0]
    if (breadcrumbs.length > 1) {
      section = breadcrumbs[1]
    }

    const props = {
      section
    }

    switch (section.id) {
      case PAGES.LECTURE_5.FILE_UPLOADS:
        return <FileUploads {...props} />
      case PAGES.LECTURE_5.ACTION_COMPOSITION:
        return <ActionComposition {...props} />
      case PAGES.LECTURE_5.PLAY_ASYNC:
        return <PlayAsync {...props} />
    }
    return <Intro {...props} />
  }
}

export default withStyles(styles)(Index)
