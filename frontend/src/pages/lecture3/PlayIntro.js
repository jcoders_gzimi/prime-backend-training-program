/**
 * Created by LeutrimNeziri on 09/04/2019.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import { Bold, Highlighted, Italic } from "presentations/Label";
import SimpleLink from "presentations/rows/SimpleLink";
import PlayStackImage from 'assets/images/lecture3/play-stack.png'
import PlayRequestReponse from 'assets/images/lecture3/play-request-response.png'
import PlayAnatomy from 'assets/images/lecture3/anatomy.png'
import Code from "presentations/Code";
import MVCImage from 'assets/images/lecture3/mvc.png'

const styles = ({ size, typography }) => ({
  root: {},
  section: {
    width: '100%',
    display: 'flex',
    alignItems: 'center'
  },
  piece: {
    flex: 1,
    display: 'flex',
    alignItems: 'flex-start',
    flexFlow: 'row wrap',
    marginRight: size.spacing * 2,
    '& :last-child': {
      marginRight: 0
    }
  }
})

const exercise = ``

const indexExample = `public CompletableFuture<Result> index() {
		return CompletableFuture.supplyAsync(() -> ok(views.html.index.render(config)), ec.current());
}`
const indexSimplifiedExample = `public Result index() {
		return ok(views.html.index.render(config));
}`

const simpleAction = `public Result index(Http.Request request) {
  return ok("Got request " + request + "!");
}`

const controllerExample = `package controllers;

import play.*;
import play.mvc.*;

public class Application extends Controller {

  public Result index() {
    return ok("It works!");
  }
}`

const actionWithParameter = `public Result index(String name) {
  return ok("Hello " + name);
}`

const resultExample = `public Result index() {
  return ok("Hello world!");
}`

const resultsExamples = `Result ok = ok("Hello world!");
Result notFound = notFound();
Result pageNotFound = notFound("<h1>Page not found</h1>").as("text/html");
Result badRequest = badRequest(views.html.form.render(formWithErrors));
Result oops = internalServerError("Oops");
Result anyStatus = status(488, "Strange response type");`

const redirectExample = `public Result index() {
  return redirect("/user/home");
}`
const tempRedirect = `public Result index() {
  return temporaryRedirect("/user/home");
}`

const routing = `GET  /               @io.training.api.controllers.HomeController.index`

class Intro extends React.Component {
  render() {
    const { classes, section } = this.props
    const introduction = section.children[0]
    const application = section.children[1]
    const project = section.children[2]
    const actionsControllersAndResponse = section.children[3]
    return (
      <Fragment>
        <Typography variant={'heading'}>
          Section 3: Play Framework
          <Typography>
            The The High Velocity Web Framework For Java and Scala. Home page: <SimpleLink href="https://www.playframework.com/">https://www.playframework.com/</SimpleLink>
          </Typography>
          <Divider />
        </Typography>

        <Typography>
          Play Framework makes it easy to build web applications with Java & Scala. It is based on a lightweight, stateless, web-friendly architecture.
        </Typography>
        <Typography>
          Built on Akka, Play provides predictable and minimal resource consumption (CPU, memory, threads) for highly-scalable applications.
        </Typography>
        <div className={classes.section}>
          <Typography className={classes.piece}>
            <Bold>Developer friendly.</Bold><br/> Make your changes and simply hit refresh! All you need is a browser and a text editor.
            <ul>
              <li>Hit refresh workflow</li>
              <li>Powerful console & build tools</li>
              <li>Type safety</li>
              <li>Built in testing tools</li>
              <li>IDE support for Eclipse and IntelliJ IDEA</li>
            </ul>
          </Typography>
          <Typography className={classes.piece}>
            <Bold>Scale predictably.</Bold><br/> Underneath the covers Play uses a fully asynchronous model built on top of Akka. Combined with being stateless, Play scales simply and predictably.
            <ul>
              <li>Stateless Web Tier</li>
              <li>Non-blocking I/O</li>
              <li>Built on Akka</li>
              <li>Real-time enabled</li>
            </ul>
          </Typography>
          <Typography className={classes.piece}>
            <Bold>Modern web & mobile.</Bold><br/> Play was built for needs of modern web & mobile apps.
            <ul>
              <li>RESTful by default</li>
              <li>Asset Compiler for CoffeeScript, LESS, etc</li>
              <li>JSON is a first class citizen</li>
              <li>Websockets, Comet, EventSource</li>
              <li>Extensive NoSQL & Big Data Support</li>
            </ul>
          </Typography>
        </div>

        <div className={classes.section}>
          <Typography className={classes.piece}>
            <Bold>Solid & fast.</Bold><br/> The compiler and runtime (JVM) do the heavy lifting so that your apps run super-fast and keeps running under load.
            <ul>
              <li>Code is compiled</li>
              <li>Runs on the JVM</li>
              <li>Java NIO via Akka HTTP or Netty</li>
            </ul>
          </Typography>
          <Typography className={classes.piece}>
            <Bold>Massive ecosystem.</Bold><br/> The ecosystem around Java is huge. There are libraries for everything - most of which can be used in Play.
            <ul>
              <li>Use Maven Central libraries</li>
              <li>Numerous Play plugins</li>
              <li>Very active Play community</li>
            </ul>
          </Typography>
          <Typography className={classes.piece}>
            <Bold>Proven in production.</Bold><br/> From startups to enterprises, Play powers some of the most innovative web sites. Play is proven, reliable and scalable.
            <ul>
              <li>Production support by Lightbend</li>
              <li>Many cloud deployment options</li>
              <li>Used in mission-critical apps</li>
            </ul>
          </Typography>
        </div>
        <Typography id={introduction.id} variant={'title'}>
          {introduction.display}
        </Typography>
        <div className={classes.section}>
          <div>
            <img src={PlayStackImage} style={{ maxWidth: 680 }} />
          </div>
          <div className={classes.piece} style={{ padding: 16}}>
            <Typography>
              As illustrated to the left, Play is a full-stack framework with all of the components you need to build a Web Application or a REST service, including: an integrated HTTP server, form handling, Cross-Site Request Forgery (CSRF) protection, a powerful routing mechanism, I18n support, and more. Play integrates with many object relational mapping (ORM) layers. It supports Anorm, Ebean, Slick, and JPA out-of-the-box, but many customers use NoSQL, other ORMs or even access data from a REST service.
            </Typography>
            <Typography>
              Play APIs are available in both Java and Scala. The Framework uses Akka and Akka HTTP under the hood. This endows Play applications with a stateless, non-blocking, event-driven architecture that provides horizontal and vertical scalability and uses resources more efficiently. Play projects contain Scala components, but because Play has a Java API, Java developers do not need to learn Scala to use Play successfully.
            </Typography>
            <Typography>
              Here are some aspects of Play that make it developer friendly:
              <ol>
                <li>Its Model-View-Controller (MVC) architecture is familiar and easy to learn.</li>
                <li>Direct support of common web development tasks and hot reloading saves precious development time.</li>
                <li>A large active community promotes knowledge sharing.</li>
                <li>Twirl templates render pages. The Twirl template language is:
                  <ol>
                    <li>Easy to learn</li>
                    <li>Requires no special editor</li>
                    <li>Provides type safety</li>
                    <li>Is compiled so that errors display in the browser</li>
                  </ol>
                </li>
              </ol>
              If you want to learn more about the phylosophy behind Play framework follow: <SimpleLink href="https://www.playframework.com/documentation/2.8.x/Philosophy">https://www.playframework.com/documentation/2.8.x/Philosophy</SimpleLink>
            </Typography>
          </div>
        </div>
        <Typography>
          Here is a simple illustration on MVC:
        </Typography>
        <img src={MVCImage} style={{ maxWidth: 360 }}/>
        <Typography id={application.id} variant={'title'}>
          {application.display}
        </Typography>
        <Typography>
          Our backend is implemented as a Play application that we can examine to start learning about Play. Let’s first look at what happens at runtime. When you enter <SimpleLink href={"http://localhost:9000/"}>http://localhost:9000/</SimpleLink> in your browser:
          <ol>
            <li>The browser requests the root / URI from the HTTP server using the GET method.</li>
            <li>The Play internal HTTP Server receives the request.</li>
            <li>Play resolves the request using the routes file, which maps URIs to controller action methods.</li>
            <li>The action method renders the index page, using Twirl templates.</li>
            <li>The HTTP server returns the response as an HTML page.</li>
          </ol>
          At a high level, the flow looks something like this:
        </Typography>
        <img src={PlayRequestReponse} style={{ maxWidth: 1280 }}/>
        <Typography>
          The same idea applies to getting other types of information from Play via a GET request like JSON data.<br/>
        </Typography>

        <Typography id={project.id} variant={'title'}>
          {project.display}
        </Typography>
        <Typography>
          Next, let’s look at the tutorial project to locate the implementation for:
          <ol>
            <li>The routes file that maps the request to the controller method.</li>
            <li>The controller action method that defines how to handle a request to the root URI.</li>
            <li>The Twirl template that the action method calls to render the HTML markup.</li>
          </ol>
        </Typography>
        <Typography>
          The layout of a Play application is standardized to keep things as simple as possible. After the first successful compilation, the project structure looks like this:
        </Typography>
        <img src={PlayAnatomy}/>
        <Typography>
          Here is the full anatomy explanation: <SimpleLink href={"https://www.playframework.com/documentation/2.8.x/Anatomy"}>https://www.playframework.com/documentation/2.8.x/Anatomy</SimpleLink>
        </Typography>
        <Typography>
          The only difference at our play app is that our app directory starts at -> <Bold>io/training/api/</Bold> Where io is the organisation, training is the platform and api is the service!
        </Typography>
        <Typography>
          In order to do that open your IDE and drill down through the backend directory (the root of our Play Application):
          <ol>
            <li>The app subdirectory contains directories for controllers and views, which will be familiar to those experienced with the Model View Controller (MVC) architecture. Since this simple project does not need an external data repository, it does not contain a models directory, but this is where you would add it.</li>
            <li>The conf directory contains application configuration. For details on the rest of the project’s structure see Anatomy of a Play Application.</li>
            <li>The public subdirectory contains directories for images, javascripts, and stylesheets. <Italic>We are not going to use this for now.</Italic></li>
          </ol>
          To locate the controller action method, open <Italic>io/goprime/training/controllers/HomeController.java</Italic>. The HomeController class includes the index action method, as shown below. This is a very simple action method that generate an HTML page from the index.scala.html Twirl template file:
          <Code>
            {indexExample}
          </Code>
          Or a simplified version
          <Code>
            {indexSimplifiedExample}
          </Code>
          To view the route that maps the browser request to the controller method, open the conf/routes file. A route consists of an HTTP method, a path, and an action. This control over the URL schema makes it easy to design clean, human-readable, bookmarkable URLs. The following line maps a GET request for the root URL / to the index action in HomeController:
          <Code>
            {routing}
          </Code>
          Open app/views/index.scala.html with your editor to see what the HTML looks like. Change it and refresh <SimpleLink href={"http://localhost:9000/"}>http://localhost:9000/</SimpleLink> to see your changes.
        </Typography>
        <Typography id={actionsControllersAndResponse.id} variant={'title'}>
          {actionsControllersAndResponse.display}
        </Typography>
        <Typography variant="section">
          Actions
        </Typography>
        <Typography>
          Most of the requests received by a Play application are handled by an action.
        </Typography>
        <Typography>
          An action is basically a Java method that processes the request parameters, and produces a result to be sent to the client.
          <Code>
            {simpleAction}
          </Code>
          An action returns a <Highlighted>play.mvc.Result</Highlighted> value, representing the HTTP response to send to the web client. In this example <Highlighted>ok</Highlighted> constructs a 200 OK response containing a text/plain response body. For more examples of HTTP responses see <SimpleLink href="https://www.playframework.com/documentation/2.8.x/api/java/play/mvc/Results.html#method.summary">play.mvc.Results</SimpleLink> methods.
        </Typography>
        <Typography variant="section">
          Controllers
        </Typography>
        <Typography>
          A controller is nothing more than a class extending <Highlighted>play.mvc.Controller</Highlighted> that groups several action methods.
          <Code>
            {controllerExample}
          </Code>
          The simplest syntax for defining an action is a method with no parameters that returns a Result value, as shown above.
        </Typography>
        <Typography>
          An action method can also have parameters:
          <Code>
            {actionWithParameter}
          </Code>
          These parameters will be resolved by the Router and will be filled with values from the request URL. The parameter values can be extracted from either the URL path or the URL query string.
        </Typography>

        <Typography variant="section">
          Results
        </Typography>
        <Typography>
          Let’s start with simple results: an HTTP result with a status code, a set of HTTP headers and a body to be sent to the web client.
        </Typography>
        <Typography>
          These results are defined by play.mvc.Result, and the play.mvc.Results class provides several helpers to produce standard HTTP results, such as the ok method we used in the previous section:
          <Code>
            {resultExample}
          </Code>
          Here are several examples that create various results:
          <Code>
            {resultsExamples}
          </Code>
          All of these helpers can be found in the <Highlighted>play.mvc.Results</Highlighted> class.
        </Typography>

        <Typography variant="section">
          Redirects are simple results too
        </Typography>
        <Typography>
          Redirecting the browser to a new URL is just another kind of simple result. However, these result types don’t have a response body.
        </Typography>
        <Typography>
          There are several helpers available to create redirect results:
          <Code>
            {redirectExample}
          </Code>
          The default is to use a <Highlighted>303 SEE_OTHER</Highlighted> response type, but you can also specify a more specific status code:
          <Code>
            {tempRedirect}
          </Code>
        </Typography>

        <Typography>
          Lets do some exercises:
          <Code>

          </Code>
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(Intro)
