/**
 * Created by LeutrimNeziri on 09/04/2019.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import PageLink from "presentations/rows/nav/PageLink";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";

const styles = ({ typography }) => ({
  root: {},
})

class Intro extends React.Component {
  render() {
    const { classes, section } = this.props
    return (
      <Fragment>
        <Typography variant={'heading'}>
          Section 7: Akka Introduction and Akka Actors
          <Divider />
        </Typography>
        <Typography variant='p'>
          The purpose of the section five is to introduce the Akka Framework and talk about Akka Actors
        </Typography>
        <Typography variant='p'>
          The section 7 contains these underlying pages:
          <ol>
            {section.children.map(next => <li key={next.id}>
              <PageLink to={`/section/${next.id}/`}>{next.display}</PageLink>
            </li>)}
          </ol>
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(Intro)
