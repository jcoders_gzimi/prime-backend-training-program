/**
 * Created by Agon Lohaj on 09/04/2019.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import { PAGES } from 'Constants';
import Intro from "pages/lecture7/Intro";
import AkkaIntro from "pages/lecture7/AkkaIntro";
import AkkaPlay from "pages/lecture7/AkkaPlay";
import React from "react";

const styles = () => ({
  root: {},
})

class Index extends React.Component {
  render() {
    const { breadcrumbs } = this.props

    let section = breadcrumbs[0]
    if (breadcrumbs.length > 1) {
      section = breadcrumbs[1]
    }

    const props = {
      section
    }

    switch (section.id) {
      case PAGES.LECTURE_7.INTRODUCTION:
        return <AkkaIntro {...props} />
      case PAGES.LECTURE_7.AKKA_PLAY:
        return <AkkaPlay {...props} />
    }
    return <Intro {...props} />
  }
}

export default withStyles(styles)(Index)
