package io.training.api.actions;

import com.google.inject.Inject;
import play.Logger;
import play.cache.AsyncCacheApi;
import play.cache.NamedCache;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

import java.util.concurrent.CompletionStage;

/**
 * Created by Agon Lohaj on 09/08/2020
 */
public class WithCacheAction extends Action<WithCache> {

	@Inject
	@NamedCache("training")
	AsyncCacheApi cacheApi;

	@Override
    public CompletionStage<Result> call(Http.Request request) {
		Logger.of(this.getClass()).debug("Cached Action for {}. Cache Api exists: {}", configuration.key(), cacheApi != null ? "Not null" : "null");
		return cacheApi.getOrElseUpdate(configuration.key(), () -> delegate.call(request));
    }
}
