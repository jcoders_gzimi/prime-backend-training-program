package io.training.api.controllers;

import com.google.inject.Inject;
import com.typesafe.config.Config;
import io.training.api.actions.Attributes;
import io.training.api.actions.TaxiProviderAction;
import io.training.api.actions.VerboseAnnotation;
import io.training.api.actions.WithCache;
import io.training.api.executors.MongoExecutionContext;
import io.training.api.models.Taxi;
import io.training.api.mongo.IMongoDB;
import play.Logger;
import play.cache.AsyncCacheApi;
import play.cache.NamedCache;
import play.libs.Json;
import play.libs.concurrent.Futures;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.*;
import scala.Option;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import play.cache.SyncCacheApi;

import static java.time.temporal.ChronoUnit.SECONDS;


public class HomeController extends Controller {
	@Inject
	Config config;

	@Inject
	HttpExecutionContext ec;

	@Inject
	MongoExecutionContext mongoExecutionContext;

	@Inject
	Futures futures;

	@Inject
	IMongoDB mongoDb;

	@VerboseAnnotation( false )
	public CompletableFuture<Result> index() {
		return CompletableFuture.supplyAsync(() -> ok(views.html.index.render(config)), ec.current());
	}

	@With( TaxiProviderAction.class )
	public Result taxi (Http.Request request) {
		Taxi taxi = request.attrs().get(Attributes.TAXI_TYPED_KEY);
		return ok(Json.toJson(taxi));
	}

	@With( TaxiProviderAction.class )
	@WithCache(key = "taxi")
	public Result cachedTaxi (Http.Request request) {
		Logger.of(this.getClass()).debug("Calculating taxi for the cache Action");
		Taxi taxi = request.attrs().get(Attributes.TAXI_TYPED_KEY);
		return ok(Json.toJson(taxi));
	}

	@Inject
	@NamedCache("redis")
	AsyncCacheApi cacheApi; // Async, lazy caching

	@Inject
	@NamedCache("redis")
	SyncCacheApi syncCacheApi; // Synchronous caching

	public CompletionStage<Result> cachedRedis (Http.Request request) {
		return cacheApi.getOrElseUpdate(
			"redis-expiry", // the key
			() -> CompletableFuture.supplyAsync(() -> "Some expensive value"), // if it doesn't exist how to update it
			20 // expiration time
		)
		.thenApply(Results::ok);
	}

	public CompletableFuture<Result> mongo() {
		return CompletableFuture.supplyAsync(() -> {
			List<Taxi> items = mongoDb
				.getMongoDatabase()
				.getCollection("taxi", Taxi.class)
				.find()
				.into(new ArrayList<>());
			return items;
		}, mongoExecutionContext).thenCompose((items) -> {
			return CompletableFuture.supplyAsync(() -> {
				return ok(Json.toJson(items));
			}, ec.current());
		});
	}

	public CompletionStage<Result> mongoWithTimeout() {
		CompletableFuture<Result> future = this.mongo();
		return futures.timeout(future, Duration.of(2, SECONDS)); // The max time to wait on response
	}

	public CompletionStage<Result> mongoDelayed() {
		CompletableFuture<Result> future = this.mongo();
		return futures.delayed(() -> future, Duration.of(3, SECONDS)); // Execute after an initial delay of 3 seconds
	}

	public Result routeNotFound () {
		return status(404);
	}

	public Result redirectTo (String from) {
		return redirect("404");
	}


	public CompletionStage<Result> test() {
		// Use a different task with explicit EC
		return calculateResponse()
				.thenApplyAsync(answer -> ok("answer was " + answer), ec.current());
	}

	private static CompletionStage<String> calculateResponse() {
		return CompletableFuture.completedFuture("42");
	}
}