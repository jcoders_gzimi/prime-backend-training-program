package io.training.api.controllers;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.stream.Materializer;
import com.fasterxml.jackson.databind.JsonNode;
import io.training.api.actors.ConfiguredActorProtocol;
import io.training.api.actors.HelloActor;
import io.training.api.actors.HelloActorProtocol;
import io.training.api.actors.MyWebSocketActor;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.WebSocket;
import scala.compat.java8.FutureConverters;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import play.libs.streams.ActorFlow;
import play.mvc.*;
import akka.actor.*;
import akka.stream.*;
import javax.inject.Inject;

import java.util.concurrent.CompletionStage;

import static akka.pattern.Patterns.ask;

@Singleton
public class LectureSevenController extends Controller {
	private final ActorRef helloActor;
	private ActorRef configuredActor;
	private final ActorSystem actorSystem;
	private final Materializer materializer;

	@Inject
	public LectureSevenController(ActorSystem system, Materializer materializer, @Named("configured-actor") ActorRef configuredActor) {
		this.helloActor = system.actorOf(HelloActor.getProps());
		this.configuredActor = configuredActor;
		this.actorSystem = system;
		this.materializer = materializer;
	}

	public CompletionStage<Result> sayHello(String name) {
		return FutureConverters.toJava(ask(helloActor, new HelloActorProtocol.SayHello(name), 1000))
			.thenApply(response -> ok((JsonNode) response));
	}

	public CompletionStage<Result> getConfig() {
		return FutureConverters.toJava(ask(configuredActor, new ConfiguredActorProtocol.GetConfig(), 1000))
			.thenApply(response -> ok((String) response));
	}

	public WebSocket socket() {
		return WebSocket.Text.accept(
				request -> ActorFlow.actorRef(MyWebSocketActor::props, actorSystem, materializer));
	}
}