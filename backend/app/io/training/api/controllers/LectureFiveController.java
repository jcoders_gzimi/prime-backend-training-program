package io.training.api.controllers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import play.libs.Files;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class LectureFiveController extends Controller {

	@Inject
	ObjectMapper mapper;

	@BodyParser.Of(BodyParser.MultipartFormData.class)
	public Result upload (Http.Request request) throws IOException {
		Http.MultipartFormData<File> body = request.body().asMultipartFormData();
		List<Http.MultipartFormData.FilePart<File>> files = body.getFiles();
		if (files.size() == 0) {
			return badRequest("Missing files");
		}
		Http.MultipartFormData.FilePart<File> file = files.get(0);
		Files.TemporaryFile raw = (Files.TemporaryFile) file.getRef();
		String fileName = file.getFilename();
		return ok(fileName);
	}

	@BodyParser.Of(BodyParser.MultipartFormData.class)
	public Result uploadJson (Http.Request request) throws IOException {
		Http.MultipartFormData<File> body = request.body().asMultipartFormData();
		List<Http.MultipartFormData.FilePart<File>> files = body.getFiles();
		if (files.size() == 0) {
			return badRequest("Missing files");
		}
		Http.MultipartFormData.FilePart<File> file = files.get(0);
		File raw = file.getFile();
		return ok(this.fileToObjectNode(raw));
	}

	public JsonNode fileToObjectNode (File which) throws IOException {
		JsonParser parser = mapper.getFactory().createParser(which);
		JsonNode config = null;
		if (parser.nextToken() == JsonToken.START_OBJECT) {
			config = mapper.readTree(parser);
		}
		parser.close();
		return config;
	}
}