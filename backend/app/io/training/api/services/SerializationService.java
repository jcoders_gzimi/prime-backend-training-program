package io.training.api.services;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import com.typesafe.config.Config;
import io.training.api.exceptions.RequestException;
import io.training.api.utils.DatabaseUtils;
import org.bson.Document;
import play.Logger;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Http;
import play.mvc.Http.Request;

import javax.inject.Singleton;
import javax.swing.text.html.Option;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.Executor;

/**
 * Created by Agon on 09/08/2020
 */
@Singleton
public class SerializationService {
    @Inject
    HttpExecutionContext ec;

    public <T> CompletableFuture<JsonNode> toJsonNode(T result) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                return Json.toJson(result);
            } catch (Exception ex) {
                ex.printStackTrace();
                throw new CompletionException(new RequestException(Http.Status.BAD_REQUEST, "parsing_exception"));
            }
        }, ec.current());
    }

    public CompletableFuture<Document> parseBody(Request request) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                JsonNode json = request.body().asJson();
                if (!json.isObject()) {
                    throw new RequestException(Http.Status.BAD_REQUEST, "invalid_parameters");
                }
                return DatabaseUtils.toDocument((ObjectNode) json);
            } catch (RequestException ex) {
                ex.printStackTrace();
                throw new CompletionException(ex);
            } catch (Exception ex) {
                ex.printStackTrace();
                throw new CompletionException(new RequestException(Http.Status.BAD_REQUEST, "parsing_exception"));
            }
        }, ec.current());
    }

    public <T> CompletableFuture<T> parseBodyOfType(Request request, Class<T> valueType) {
        return CompletableFuture.supplyAsync(() -> this.syncParseBodyOfType(request, valueType), ec.current());
    }

    public <T> T syncParseBodyOfType (Request request, Class<T> valueType) {
        try {
            Optional<T> body = request.body().parseJson(valueType);
            if (!body.isPresent()) {
                throw new RequestException(Http.Status.BAD_REQUEST, "parsing_exception");
            }
            return body.get();
        } catch (RequestException ex) {
            ex.printStackTrace();
            throw new CompletionException(ex);
        } catch (Exception e) {
            e.printStackTrace();
            throw new CompletionException(new RequestException(Http.Status.INTERNAL_SERVER_ERROR, "service_unavailable"));
        }
    }

    public CompletableFuture<List<Document>> parseListBody(Request request) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                JsonNode json = request.body().asJson();
                if (!json.isArray()) {
                    throw new RequestException(Http.Status.BAD_REQUEST, "invalid_parameters");
                }
                return DatabaseUtils.toListDocument((ArrayNode) json);
            } catch (RequestException ex) {
                ex.printStackTrace();
                throw new CompletionException(ex);
            } catch (Exception ex) {
                ex.printStackTrace();
                throw new CompletionException(new RequestException(Http.Status.BAD_REQUEST, "parsing_exception"));
            }
        }, ec.current());
    }

    public <T> CompletableFuture<List<T>> parseListBodyOfType (Request request, Class<T> type) {
        return CompletableFuture.supplyAsync(() -> this.syncParseListBodyOfType(request, type), ec.current());
    }

    public <T> List<T> syncParseListBodyOfType (Request request, Class<T> type) {
        try {
            JsonNode json = request.body().asJson();
            if (!json.isArray()) {
                throw new RequestException(Http.Status.BAD_REQUEST, "invalid_parameters");
            }
            List<T> list = new ArrayList<>();
            for (JsonNode node: json) {
                list.add(Json.fromJson(node, type));
            }
            return list;
        } catch (RequestException | ClassCastException ex) {
            ex.printStackTrace();
            throw new CompletionException(ex);
        } catch (CompletionException ex) {
            ex.printStackTrace();
            throw ex;
        } catch (Exception e) {
            e.printStackTrace();
            throw new CompletionException(new RequestException(Http.Status.INTERNAL_SERVER_ERROR, "service_unavailable"));
        }
    }

}
