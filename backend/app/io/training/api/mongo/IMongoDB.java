package io.training.api.mongo;

import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import com.mongodb.client.MongoDatabase;

/**
 * Created by Agon on 09/08/2020.
 */
public interface IMongoDB {
	public MongoDatabase getMongoDatabase();

	public <T> CompletableFuture<T> supplyAsync(Function<MongoDatabase, CompletableFuture<T>> function);

	public CompletableFuture<MongoDatabase> getMongoDatabaseAsync();
}
