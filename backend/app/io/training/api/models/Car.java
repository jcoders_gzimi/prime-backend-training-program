package io.training.api.models;
import io.training.api.models.validators.CaseMode;
import io.training.api.models.validators.CheckCase;
import lombok.*;

import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.List;

@ToString
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Car {
	@NotEmpty(message = "Cannot be empty!")
	private String manufacturer;

	@NotEmpty(message = "License plate cannot be empty!")
	@Size(min = 2, max = 14, message = "The length should be between 2 and 14")
	@CheckCase(value = CaseMode.UPPER)
	private String licensePlate;

	@Min(value = 2, message = "Cannot be lower than 2")
	@Max(value = 100, message = "Cannot be higher than 100")
	private int seatCount;

	@AssertTrue(message = "The car must be up and running in order to work!")
	private boolean upAndRunning;

	private List<String> tags = new ArrayList<>();

	@Size(min = 1, message = "There should be at least one available!")
	public List<String> getTags() {
		return tags;
	}
}
