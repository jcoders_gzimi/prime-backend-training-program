package io.training.api.models;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by agonlohaj on 28 Aug, 2020
 */
@Data
@EqualsAndHashCode(of = "id")
public class User {
	String id;
	// TODO: add the rest of the properties
}
