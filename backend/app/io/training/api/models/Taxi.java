package io.training.api.models;
import lombok.*;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "")
@AllArgsConstructor
@NoArgsConstructor
public @Data class Taxi extends BaseModel {
	private String company;
}
