package io.training.api.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by agonlohaj on 28 Aug, 2020
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BinaryTree {
	Integer value;
	BinaryTree left;
	BinaryTree right;
}
