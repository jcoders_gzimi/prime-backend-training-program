package io.training.api.models.validators;

/**
 * Created by agonlohaj on 31 Aug, 2020
 */
public enum CaseMode {
	UPPER,
	LOWER;
}
